{
  "extends": [
    "stylelint-config-standard",
    "stylelint-config-recommended-scss",
    "stylelint-config-prettier",
    "stylelint-config-rational-order"
  ],
  "plugins": [
    "stylelint-scss",
    "stylelint-config-rational-order/plugin"
  ],
  "rules": {
    "order/properties-order": [],
    "plugin/rational-order": [
      true,
      {
        "border-in-box-model": false,
        "empty-line-between-groups": false
      }
    ],
    "font-family-name-quotes": "always-where-recommended",
    "scss/function-quote-no-quoted-strings-inside": true,
    "function-url-quotes": [
      "always",
      {
        "except": ["empty"]
      }
    ],
    "function-comma-space-after": "always-single-line",
    "function-comma-space-before": "never-single-line",
    "function-max-empty-lines": 0,
    "function-parentheses-newline-inside": "always-multi-line",
    "function-parentheses-space-inside": "never-single-line",
    "function-whitespace-after": "always",
    "number-leading-zero": "always",
    "number-no-trailing-zeros": true,
    "string-quotes": "double",
    "unit-case": "lower",
    "value-keyword-case": [
      "lower",
      {
        "ignoreKeywords": ["Arial"],
        "ignoreProperties": ["font-family"]
      }
    ],
    "value-list-comma-newline-after": "always-multi-line",
    "value-list-comma-newline-before": "never-multi-line",
    "value-list-comma-space-after": "always-single-line",
    "value-list-comma-space-before": "never",
    "value-list-max-empty-lines": 0,
    "custom-property-empty-line-before": [
      "always",
      {
        "except": ["first-nested"]
      }
    ],
    "property-case": "lower",
    "declaration-colon-newline-after": "always-multi-line",
    "declaration-colon-space-after": "always-single-line",
    "declaration-colon-space-before": "never",
    "declaration-block-semicolon-newline-after": "always",
    "declaration-block-semicolon-newline-before": "never-multi-line",
    "declaration-block-trailing-semicolon": "always",
    "block-closing-brace-empty-line-before": "never",
    "block-closing-brace-newline-after": [
      "always",
      {
        "ignoreAtRules": ["if", "else"]
      }
    ],
    "block-closing-brace-newline-before": "always",
    "block-opening-brace-space-before": "always",
    "block-opening-brace-newline-after": "always",
    "selector-combinator-space-after": "always",
    "selector-combinator-space-before": "always",
    "selector-descendant-combinator-no-non-space": true,
    "selector-pseudo-class-case": "lower",
    "selector-pseudo-element-case": "lower",
    "selector-list-comma-newline-after": "always",
    "selector-list-comma-newline-before": "never-multi-line",
    "selector-list-comma-space-after": "always-single-line",
    "selector-list-comma-space-before": "never",
    "rule-empty-line-before": [
      "always",
      {
        "except": ["first-nested", "after-single-line-comment"]
      }
    ],
    "media-feature-colon-space-after": "always",
    "media-feature-colon-space-before": "never",
    "media-feature-name-case": "lower",
    "media-feature-name-no-vendor-prefix": true,
    "media-feature-name-no-unknown": true,
    "media-query-list-comma-newline-after": "always-multi-line",
    "media-query-list-comma-newline-before": "never-multi-line",
    "media-query-list-comma-space-before": "never",
    "at-rule-empty-line-before": [
      "always",
      {
        "ignore": ["first-nested", "after-comment", "blockless-after-same-name-blockless"],
        "ignoreAtRules": ["if", "else"]
      }
    ],
    "at-rule-name-case": "lower",
    "at-rule-semicolon-newline-after": "always",
    "indentation": 2,
    "max-nesting-depth": 3,
    "selector-attribute-quotes": "always",
    "selector-max-compound-selectors": 3,
    "selector-max-specificity": "0,3,2",
    "selector-max-empty-lines": 1,
    "declaration-no-important": true,
    "at-rule-no-vendor-prefix": true,
    "property-no-vendor-prefix": [
      true,
      {
        "ignoreProperties": ["appearance"]
      }
    ],
    "property-no-unknown": [
      true,
      {
        "ignoreProperties": ["font-smoothing"]
      }
    ],
    "selector-no-vendor-prefix": true,
    "value-no-vendor-prefix": true,
    "no-empty-source": null,
    "selector-class-pattern": "^(?:(?:o|c|u|t|s|is|has|_|js|qa)-)?[a-z-0-9]+(?:-[a-z-0-9]+)*(?:__[a-z-0-9]+(?:-[a-z-0-9]+)*)?(?:--[a-z-0-9]+(?:-[a-z-0-9]+)*)?(?:\\[.+\\])?$",
    "selector-id-pattern": "[a-z-]+",
    "selector-max-id": 0,
    "selector-no-qualifying-type": [
      true,
      {
        "ignore": ["attribute", "class"]
      }
    ],
    "selector-max-universal": 1,
    "selector-pseudo-element-no-unknown": true,
    "unit-whitelist": ["px", "%", "em", "rem", "vw", "vh", "vmin", "vmax", "deg", "s", "dppx", "dpi", "fr", "x"],
    "unit-no-unknown": [
      true,
      {
        "ignoreUnits": ["x"]
      }
    ],
    "function-calc-no-invalid": true,
    "no-extra-semicolons": true,
    "color-named": "never",
    "function-url-no-scheme-relative": true,
    "number-max-precision": 2,
    "time-min-milliseconds": 100,
    "shorthand-property-no-redundant-values": true,
    "comment-word-blacklist": "/^[а-яА-Я]+/",
    "color-hex-case": "lower",
    "max-empty-lines": 1,
    "max-line-length": [
      120,
      {
        "ignore": ["non-comments"]
      }
    ],
    "no-missing-end-of-source-newline": true,
    "no-empty-first-line": true
  }
}
