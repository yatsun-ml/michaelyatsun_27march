export interface ArrayOfObjects {
  id: number;
  [propName: string]: any;
}
