import { ArrayOfObjects } from './model/helpers.model';

export function sortArrayOfObjectsByKey(arr: ArrayOfObjects[]): ArrayOfObjects[] {
  return arr.sort((a, b) => (a.id > b.id ? 1 : -1));
}
