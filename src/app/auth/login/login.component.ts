import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AccountInfo, SignInForm, SignInFormModel } from './model/login.model';
import { LocalStorageService } from '../../core/storage/storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  showNameRequiredError: boolean;
  showEmailRequiredError: boolean;
  showEmailValidationError: boolean;
  loginFormValue: AccountInfo;

  get nameInputRequiredError() {
    return this.form.get('name').hasError('required');
  }
  get emailInputRequiredError() {
    return this.form.get('email').hasError('required');
  }
  get emailInputValidationError() {
    return this.form.get('email').hasError('email');
  }

  constructor(
    private localStorageService: LocalStorageService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.showNameRequiredError = false;
    this.showEmailRequiredError = false;
    this.showEmailValidationError = false;
    this.loginFormValue = {
      name: '',
      email: '',
    };
    this.form = this.formBuilder.group(new SignInFormModel(new SignInForm()));
  }

  ngOnInit(): void {}

  login() {
    if (this.form.invalid) {
      this.showNameRequiredError = this.nameInputRequiredError;
      this.showEmailRequiredError = this.emailInputRequiredError;
      this.showEmailValidationError = this.emailInputValidationError;
      return;
    }

    this.localStorageService.setItemAccountInfoOnLocalStorage(this.loginFormValue);
    this.router.navigate(['/dashboard'], { replaceUrl: true });
  }
}
