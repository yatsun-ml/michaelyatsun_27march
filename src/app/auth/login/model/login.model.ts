import { Validators } from '@angular/forms';

export interface AccountInfo {
  name: string;
  email: string;
}

export class SignInForm {
  name: string;
  email: string;
}

export class SignInFormModel {
  constructor(
    model: SignInForm,
    public name: string | Validators | Validators[] = [model.name, Validators.required],
    public email: string | Validators | Validators[] = [model.email, [Validators.required, Validators.email]]
  ) {}
}
