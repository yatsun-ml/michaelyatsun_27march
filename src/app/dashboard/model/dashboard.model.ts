export enum StarColor {
  Grey = '../../../../assets/img/icons/grey-star.svg',
  Golden = '../../../../assets/img/icons/golden-star.svg',
}

export interface Movie {
  id: number;
  name: string;
  img: string;
  description: string;
  year: string;
  genres: string[];
  director: string;
  starring: string[];
}

export interface FavoriteListMovieItem {
  id: number;
  filmsName: string;
}
