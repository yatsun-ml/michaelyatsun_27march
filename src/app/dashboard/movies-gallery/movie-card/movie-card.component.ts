import { Component, Input, OnInit } from '@angular/core';
import { Movie, StarColor } from '../../model/dashboard.model';
import { LocalStorageService } from '../../../core/storage/storage.service';

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.scss'],
})
export class MovieCardComponent implements OnInit {
  @Input() item: Movie;
  constructor(private localStorageService: LocalStorageService) {}

  ngOnInit(): void {
    this.setStarColor(this.item.id);
  }

  setFilmsStatus(id: number, name: string): void {
    this.localStorageService.isCurrentFilmInLocalStorage(id)
      ? this.localStorageService.removeChosenFilmFromLocalStorage(id)
      : this.localStorageService.setItemFilmOnLocalStorage(id, name);
  }

  setStarColor(id): string {
    return this.localStorageService.isCurrentFilmInLocalStorage(id) ? StarColor.Golden : StarColor.Grey;
  }
}
