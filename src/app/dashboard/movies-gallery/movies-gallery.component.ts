import { Component, OnDestroy, OnInit } from '@angular/core';
import { RequestsService } from '../../core/http/requests/requests.service';
import { Movie } from '../model/dashboard.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-movies-gallery',
  templateUrl: './movies-gallery.component.html',
  styleUrls: ['./movies-gallery.component.scss'],
})
export class MoviesGalleryComponent implements OnInit, OnDestroy {
  movies: Movie[];
  subscription: Subscription;
  constructor(private movieList: RequestsService) {}

  ngOnInit(): void {
    this.subscription = this.movieList.getMoviesList().subscribe(movies => (this.movies = movies));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
