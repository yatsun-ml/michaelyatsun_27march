import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from '../../core/storage/storage.service';
import { FavoriteListService } from '../../core/services/favorite-list.service';
import { FavoriteListMovieItem } from '../model/dashboard.model';

@Component({
  selector: 'app-favorite-list',
  templateUrl: './favorite-list.component.html',
  styleUrls: ['./favorite-list.component.scss'],
})
export class FavoriteListComponent implements OnInit {
  accountName: string;
  favoriteList: FavoriteListMovieItem[];
  constructor(private localStorageService: LocalStorageService, private favoriteListService: FavoriteListService) {}

  ngOnInit(): void {
    this.accountName = this.localStorageService.getItemAccountNameFromLocalStorage();
    this.favoriteList = this.favoriteListService.favoriteList;
  }

  removeFilmFromFavoriteList(id: number): void {
    this.localStorageService.removeChosenFilmFromLocalStorage(id);
  }
}
