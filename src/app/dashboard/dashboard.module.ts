import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { MoviesGalleryComponent } from './movies-gallery/movies-gallery.component';
import { FavoriteListComponent } from './favorite-list/favorite-list.component';
import { MovieCardComponent } from './movies-gallery/movie-card/movie-card.component';

@NgModule({
  declarations: [DashboardComponent, MoviesGalleryComponent, FavoriteListComponent, MovieCardComponent],
  imports: [CommonModule, DashboardRoutingModule],
  providers: [],
})
export class DashboardModule {}
