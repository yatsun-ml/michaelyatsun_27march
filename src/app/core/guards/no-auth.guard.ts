import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LocalStorageService } from '../storage/storage.service';

@Injectable({
  providedIn: 'root',
})
export class NoAuthGuard implements CanActivate {
  constructor(private localStorageService: LocalStorageService, private router: Router) {}
  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const notAuthenticated = !this.localStorageService.isAccountAuthenticated();

    if (notAuthenticated) {
      return true;
    }

    this.router.navigate(['/dashboard'], { replaceUrl: true });

    return false;
  }
}
