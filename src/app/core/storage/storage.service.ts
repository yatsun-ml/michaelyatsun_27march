import { Injectable } from '@angular/core';
import { AccountInfo } from '../../auth/login/model/login.model';
import { FavoriteListService } from '../services/favorite-list.service';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  accountInfoKey: string;
  constructor(private favoriteListService: FavoriteListService) {
    this.accountInfoKey = 'accountInfo';
  }

  public setItemAccountInfoOnLocalStorage(accountInfo: AccountInfo): void {
    localStorage.setItem(this.accountInfoKey, JSON.stringify(accountInfo));
  }
  public isAccountAuthenticated(): boolean {
    return !!localStorage.getItem(this.accountInfoKey);
  }
  public getItemAccountNameFromLocalStorage(): string {
    return JSON.parse(localStorage.getItem(this.accountInfoKey)).name;
  }
  public setItemFilmOnLocalStorage(id: number, filmsName: string): void {
    localStorage.setItem(`Film-${id}`, filmsName);
    this.favoriteListService.addFilmToFavoriteList({ id, filmsName });
  }
  public isCurrentFilmInLocalStorage(id: number): boolean {
    return !!localStorage.getItem(`Film-${id}`);
  }
  public removeChosenFilmFromLocalStorage(id: number): void {
    localStorage.removeItem(`Film-${id}`);
    this.favoriteListService.removeFilmFromFavoriteList(id);
  }
}
