import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Movie } from '../../../dashboard/model/dashboard.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class RequestsService {
  constructor(private http: HttpClient) {}

  getMoviesList(): Observable<Movie[]> {
    return this.http.get<Movie[]>('http://my-json-server.typicode.com/moviedb-tech/movies/list');
  }
}
