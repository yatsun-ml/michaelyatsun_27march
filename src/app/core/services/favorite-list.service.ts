import { Injectable } from '@angular/core';
import { FavoriteListMovieItem } from '../../dashboard/model/dashboard.model';
import { sortArrayOfObjectsByKey } from '../../../helpers/helpers';

@Injectable()
export class FavoriteListService {
  favoriteList: FavoriteListMovieItem[] = [];
  constructor() {
    this.fillFavoriteList();
  }

  fillFavoriteList(): void {
    const regStr = /Film-/;

    Object.keys(localStorage).forEach(key => {
      if (key.match(regStr)) {
        const moviesId = +key.replace(regStr, '');
        const movieTitle = localStorage.getItem(`Film-${moviesId}`);
        const movieItem = {
          id: moviesId,
          filmsName: movieTitle,
        };
        this.favoriteList.push(movieItem);
      }
    });
    sortArrayOfObjectsByKey(this.favoriteList);
  }

  addFilmToFavoriteList(film: FavoriteListMovieItem): void {
    this.favoriteList.push(film);
    sortArrayOfObjectsByKey(this.favoriteList);
  }

  removeFilmFromFavoriteList(id: number): void {
    const filmsPositionInArray = this.favoriteList.indexOf(this.favoriteList.find(el => el.id === id));
    this.favoriteList.splice(filmsPositionInArray, 1);
  }
}
